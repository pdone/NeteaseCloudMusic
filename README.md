# 网易云音乐每日听歌300首

### 用途
该项目用于提升网易云音乐账号听歌数量，从而提升账号等级。
[https://www.52pojie.cn/thread-1197930-1-1.html](https://www.52pojie.cn/thread-1197930-1-1.html)

### 计划
- <s>增加Webkit内核</s>

### 版本记录
#### v1.3.1
- 增加使用说明
#### v1.3.0
- 增加最小化到系统托盘
#### v1.2.0
- 增加打卡网站配置
#### v1.1.0
- 移除api.php页面中的follow方法中关注接口的ID参数  将follow后的参数改为0  就不会关注任何人了
```
public function follow(){
    $url="https://music.163.com/weapi/user/follow/0";
    return '{"code":'.json_decode($this->curl($url,$this->prepare(array('csrf_token'=>$_COOKIE["__csrf"]))),1)["code"].'}';}
```
- 增加执行日志
- 增加定时执行功能
- 修复bug
#### v1.0.0
- 初始版本
### FAQ
#### 网易云音乐账号等级有何作用？
作用不大，我想升级的原因是马上满级，想看看满级是什么样子的。
另外升级可以提升音乐云盘的空间。
![等级特权图片](https://images.gitee.com/uploads/images/2020/0615/142927_37a0b300_2157511.png)