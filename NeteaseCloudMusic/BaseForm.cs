﻿using NeteaseCloudMusic.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace NeteaseCloudMusic
{
    public partial class BaseForm : Form
    {
        public BaseForm()
        {
            InitializeComponent();
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Opacity = 0.9D;

        }

        #region Esc关闭窗口
        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            int WM_KEYDOWN = 256;
            int WM_SYSKEYDOWN = 260;
            if (msg.Msg == WM_KEYDOWN | msg.Msg == WM_SYSKEYDOWN)
            {
                switch (keyData)
                {
                    case Keys.Escape:
                        this.Close();//esc关闭窗体
                        break;
                }
            }
            return false;
        }
        #endregion

        #region 无边框拖动效果
        [DllImport("user32.dll")]//拖动无窗体的控件
        public static extern bool ReleaseCapture();
        [DllImport("user32.dll")]
        public static extern bool SendMessage(IntPtr hwnd, int wMsg, int wParam, int lParam);
        public const int WM_SYSCOMMAND = 0x0112;
        public const int SC_MOVE = 0xF010;
        public const int HTCAPTION = 0x0002;

        /// <summary>
        /// 拖动窗体
        /// </summary>
        public void DragWindow()
        {
            ReleaseCapture();
            SendMessage(this.Handle, WM_SYSCOMMAND, SC_MOVE + HTCAPTION, 0);
        }
        #endregion

        private void BaseForm_MouseDown(object sender, MouseEventArgs e)
        {
            DragWindow();
        }

        /// <summary>
        /// 打开路径或网址
        /// </summary>
        /// <param name="path"></param>
        public void OpenPath(string path)
        {
            try
            {
                System.Diagnostics.Process.Start(path);
            }
            catch(Exception ex)
            {
                LogHelper.Error(ex);
                MessageBox.Show(ex.Message);
            }
        }
    }
}
