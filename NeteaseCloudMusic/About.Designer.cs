﻿namespace NeteaseCloudMusic
{
    partial class About
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(About));
            this.label1 = new System.Windows.Forms.Label();
            this.linkConfig = new System.Windows.Forms.LinkLabel();
            this.button1 = new System.Windows.Forms.Button();
            this.linkWebSource = new System.Windows.Forms.LinkLabel();
            this.linkFormSource = new System.Windows.Forms.LinkLabel();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑 Light", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(35, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(287, 200);
            this.label1.TabIndex = 0;
            this.label1.Text = resources.GetString("label1.Text");
            this.label1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.label1_MouseDown);
            // 
            // linkConfig
            // 
            this.linkConfig.AutoSize = true;
            this.linkConfig.Font = new System.Drawing.Font("楷体", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.linkConfig.Location = new System.Drawing.Point(62, 241);
            this.linkConfig.Name = "linkConfig";
            this.linkConfig.Size = new System.Drawing.Size(223, 15);
            this.linkConfig.TabIndex = 1;
            this.linkConfig.TabStop = true;
            this.linkConfig.Text = "%AppData%\\NeteaseCloudMusic";
            this.linkConfig.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkConfig_LinkClicked);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.button1.BackgroundImage = global::NeteaseCloudMusic.Properties.Resources.close;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Location = new System.Drawing.Point(338, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(25, 25);
            this.button1.TabIndex = 3;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // linkWebSource
            // 
            this.linkWebSource.AutoSize = true;
            this.linkWebSource.Font = new System.Drawing.Font("楷体", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.linkWebSource.Location = new System.Drawing.Point(29, 201);
            this.linkWebSource.Name = "linkWebSource";
            this.linkWebSource.Size = new System.Drawing.Size(303, 15);
            this.linkWebSource.TabIndex = 4;
            this.linkWebSource.TabStop = true;
            this.linkWebSource.Text = "https://pdone.lanzous.com/iOjhUdp1tgj";
            this.linkWebSource.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkWebSource_LinkClicked);
            // 
            // linkFormSource
            // 
            this.linkFormSource.AutoSize = true;
            this.linkFormSource.Font = new System.Drawing.Font("楷体", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.linkFormSource.Location = new System.Drawing.Point(16, 161);
            this.linkFormSource.Name = "linkFormSource";
            this.linkFormSource.Size = new System.Drawing.Size(335, 15);
            this.linkFormSource.TabIndex = 5;
            this.linkFormSource.TabStop = true;
            this.linkFormSource.Text = "https://gitee.com/pdone/NeteaseCloudMusic";
            this.linkFormSource.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkFormSource_LinkClicked);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(21, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(324, 20);
            this.label2.TabIndex = 6;
            this.label2.Text = "本程序仅用于学习与交流，严禁用于商业用途！";
            // 
            // About
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(366, 272);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.linkFormSource);
            this.Controls.Add(this.linkWebSource);
            this.Controls.Add(this.linkConfig);
            this.Controls.Add(this.label1);
            this.Name = "About";
            this.Text = "关于";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel linkConfig;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.LinkLabel linkWebSource;
        private System.Windows.Forms.LinkLabel linkFormSource;
        private System.Windows.Forms.Label label2;
    }
}