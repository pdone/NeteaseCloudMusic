﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace NeteaseCloudMusic
{
    public partial class Warning : BaseForm
    {
        public Warning()
        {
            InitializeComponent();
            timer = new Timer
            {
                Enabled = true,
                Interval = 1000
            };
            timer.Tick += Timer_Tick;
            timer.Start();
            button1.Text = $"确定({countdown}s)";
            button1.Enabled = false;
        }
        int countdown = 5;
        private void Timer_Tick(object sender, EventArgs e)
        {
            if (countdown > 1)
            {
                countdown--;
                button1.Text = $"确定({countdown}s)";
            }
            else
            {
                button1.Text = $"确定";
                button1.Enabled = true;
                button1.Cursor = Cursors.Default;
                timer.Enabled = false;
            }
        }

        Timer timer;
        private void label1_MouseDown(object sender, MouseEventArgs e)
        {
            DragWindow();
        }

        private void label3_MouseDown(object sender, MouseEventArgs e)
        {
            DragWindow();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();

        }
    }
}
