﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace NeteaseCloudMusic
{
    public partial class About : BaseForm
    {
        public About()
        {
            InitializeComponent();
        }

        private void label1_MouseDown(object sender, MouseEventArgs e)
        {
            DragWindow();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
        }

        private void linkConfig_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var temp = Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
           "NeteaseCloudMusic");
            OpenPath(temp);
        }

        private void linkFormSource_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            OpenPath("https://gitee.com/pdone/NeteaseCloudMusic");
        }

        private void linkWebSource_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            OpenPath("https://pdone.lanzous.com/iOjhUdp1tgj");
        }
    }
}
