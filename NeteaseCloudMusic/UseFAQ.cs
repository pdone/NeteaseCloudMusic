﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NeteaseCloudMusic
{
    public partial class UseFAQ : BaseForm
    {
        public UseFAQ()
        {
            InitializeComponent();
            this.Opacity = 0.95;
        }

        private void label1_MouseDown(object sender, MouseEventArgs e)
        {
            DragWindow();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
