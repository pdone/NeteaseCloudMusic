﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace NeteaseCloudMusic.Utils
{
    public static class IniHelper
    {
        public static string filePath
        {
            get
            {
                var temp = Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    "NeteaseCloudMusic");
                if (!Directory.Exists(temp))
                {
                    Directory.CreateDirectory(temp);
                }
                return Path.Combine(temp, "config.ini");
            }
            set
            {
                filePath = value;
            }
        }

        /// <summary>
        /// 默认节点名
        /// </summary>
        public static string defaultSection { get; set; } = "root";

        // 声明INI文件的写操作函数 WritePrivateProfileString()      
        [System.Runtime.InteropServices.DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);

        /// <summary>
        /// 给配置文件里写字符串
        /// </summary>
        /// <param name="section">段名称</param>
        /// <param name="key">键名称</param>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static void WriteSetting(string section, string key, string value)
        {
            WritePrivateProfileString(section, key, value, filePath);
        }

        public static void WriteSetting(string key, string value)
        {
            WritePrivateProfileString(defaultSection, key, value, filePath);
        }

        /// <summary>
        /// 给配置文件里写整型值
        /// </summary>
        /// <param name="section">段名称</param>
        /// <param name="key">键名称</param>
        /// <param name="value">值</param>
        public static void WriteSetting(string section, string key, int value)
        {
            WritePrivateProfileString(section, key, value.ToString(), filePath);
        }

        public static void WriteSetting(string key, int value)
        {
            WritePrivateProfileString(defaultSection, key, value.ToString(), filePath);
        }

        /// <summary>
        /// 给配置文件里写布尔值
        /// </summary>
        /// <param name="section">段名称</param>
        /// <param name="key">键名称</param>
        /// <param name="value">值</param>
        public static void WriteSetting(string section, string key, bool value)
        {
            WritePrivateProfileString(section, key, value.ToString(), filePath);
        }

        public static void WriteSetting(string key, bool value)
        {
            WritePrivateProfileString(defaultSection, key, value.ToString(), filePath);
        }

        /// <summary>
        /// 给配置文件里写浮点值
        /// </summary>
        /// <param name="section">段名称</param>
        /// <param name="key">键名称</param>
        /// <param name="value">值</param>
        /// <param name="filePath">配置文件全路径</param>
        public static void WriteSetting(string section, string key, double value)
        {
            WritePrivateProfileString(section, key, value.ToString(), filePath);
        }

        public static void WriteSetting(string key, double value)
        {
            WritePrivateProfileString(defaultSection, key, value.ToString(), filePath);
        }

        // 声明INI文件的读操作函数 GetPrivateProfileString()
        [System.Runtime.InteropServices.DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, System.Text.StringBuilder retVal, int size, string filePath);

        /// <summary>
        /// 从配置文件里获取一个值。
        /// </summary>
        /// <param name="section">段名称</param>
        /// <param name="key">键名称</param>        
        /// <param name="valueDef">默认值</param>
        /// <returns>得到的返回值</returns>
        public static string GetSetting(string Key, string Section = "", string valueDef = "")
        {
            if (!System.IO.File.Exists(filePath))
            {
                return valueDef;
            }

            Section = Section.IsNullOrWhiteSpace() ? defaultSection : Section;

            int cap = 3 * 4096;
            string valueStr = valueDef;

            StringBuilder temp = new StringBuilder(cap);
            int i = GetPrivateProfileString(Section, Key, "", temp, cap, filePath);
            if (i != 0)
                valueStr = temp.ToString();

            return valueStr;
        }

        /// <summary>
        /// 从配置文件里获取一个值。
        /// </summary>
        /// <param name="section">段名称</param>
        /// <param name="key">键名称</param>        
        /// <param name="valueDef">默认值</param>
        /// <returns>得到的返回值</returns>
        public static DateTime GetDateSetting(string Key, string Section = "", string valueDef = "")
        {
            var resDate = DateTime.MaxValue;
            if (!System.IO.File.Exists(filePath))
            {
                return resDate;
            }
            Section = Section.IsNullOrWhiteSpace() ? defaultSection : Section;

            int cap = 3 * 4096;
            string valueStr = valueDef;

            StringBuilder temp = new StringBuilder(cap);
            int i = GetPrivateProfileString(Section, Key, "", temp, cap, filePath);
            if (i != 0)
                valueStr = temp.ToString();

            DateTime.TryParseExact(valueStr, "yyyyMMddHHmmss", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None
                , out resDate);
            return resDate;
        }

        /// <summary>
        /// 从配置文件里获取一个值。
        /// </summary>
        /// <param name="section">段名称</param>
        /// <param name="key">键名称</param>    
        ///<param name="valueDef">默认值</param>
        /// <returns>得到的返回值</returns>
        public static int GetIntSetting(string Key, string Section = "", int valueDef = 0)
        {
            Section = Section.IsNullOrWhiteSpace() ? defaultSection : Section;
            string valStr = valueDef.ToString();
            valStr = GetSetting(Key, Section, valStr);

            int value = valueDef;
            int.TryParse(valStr, out value);
            return value;
        }

        /// <summary>
        /// 从配置文件里获取一个值。
        /// </summary>
        /// <param name="section">段名称</param>
        /// <param name="key">键名称</param>    
        ///<param name="valueDef">默认值</param>
        /// <returns>得到的返回值</returns>
        public static bool GetBoolSetting(string Key, string Section = "", bool valueDef = false)
        {
            Section = Section.IsNullOrWhiteSpace() ? defaultSection : Section;
            string valStr = "false";
            valStr = GetSetting(Key, Section, valStr);

            bool value = valueDef;
            bool.TryParse(valStr, out value);
            return value;
        }

        /// <summary>
        /// 从配置文件里获取一个值。
        /// </summary>
        /// <param name="section">段名称</param>
        /// <param name="key">键名称</param>    
        ///<param name="valueDef">默认值</param>
        /// <returns>得到的返回值</returns>
        public static double GetDoubleSetting(string Key, string Section = "", double valueDef = 0)
        {
            Section = Section.IsNullOrWhiteSpace() ? defaultSection : Section;
            string valStr = valueDef.ToString();
            valStr = GetSetting(Key, Section, valStr);

            double value = valueDef;
            double.TryParse(valStr, out value);
            return value;
        }
    }
}
